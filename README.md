###In this exercise you're going to use everything you've learned about rows and columns to structure your own mini website page.

###Look at the example above. I wan't you to really look at this and visualize how the rows and columns are being structured to place the content on the page.

###For your convenience, here's the row and column classes.

###.row {   display: flex; }  
###.col-1 {width: 8.33%;} 
###.col-2 {width: 16.66%;} 
###.col-3 {width: 25%;} 
###.col-4 {width: 33.33%;} 
###.col-5 {width: 41.66%;} 
###.col-6 {width: 50%;} 
###.col-7 {width: 58.33%;} 
###.col-8 {width: 66.66%;} 
###.col-9 {width: 75%;} 
###.col-10 {width: 83.33%;} 
###.col-11 {width: 91.66%;} 
###.col-12 {width: 100%;}

##Here's some guidelines:
###1. Create one rule to center all <h1> <h2> <h4> and <p> tags.
###2. Color ONLY <h1> tags.
###3. Find an image and resize it if needed so it fits nicely on page
###4. Add some background colors like the example.
###5. Add a Google Font to any text of your choice.
###6. Lets get tricky. At the bottom of the page, using {row} and {grid} Classes - place the text as displayed above.
